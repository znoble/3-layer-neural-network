#!/usr/bin/env python3

import sys
import os
import numpy as np
from random import randrange,shuffle
from scipy.special import expit

debug = False

if __name__ == "__main__":

    # read image files for testing
    test = np.load("testSet.npy")
    solutions = ["shoe", "airplane", "coffee_cup", "bicycle", "spoon"]

# information for finding error in network
# guess[number] = [#, #, #, #, #] < each # is the number of times it guessed each image. pos is ref to solutions dict
    guesses = {}
    for i in range(5):
        guesses[i] = [0] * 5

# import weights matrices from learn.py

    w1 = np.load("L1-weights.npy")

    w2 = np.load("L2-weights.npy")

#
# test the trained network on data it has never seen
#
    right = wrong = 0
    total_right = total_wrong = 0
    print("Size of Testing Data: ", test.shape[1])
    print("***Testing Data***")
    for i in range(5):
        for j in range(test.shape[1]):
            x = test[i, j]
            y = np.zeros(5)             # make a vector of zeros
            y[i] = 1.0       # set the answer component to 1

            # 1st Layer
            
            z1 = np.matmul(w1, x)
            v1 = expit(z1)
            
            # Final Layer
            
            z2 = np.matmul(w2, v1)       # forward propagation to layer 2
            a = expit(z2)                # logistic function

            guess = np.argmax(a)
            
            if guess == i:
                right += 1
            else:
                wrong += 1

            guesses[i][guess] += 1

        print(solutions[i], right, wrong, right/(right+wrong))
        total_right += right
        total_wrong += wrong
        right = wrong = 0

    print("\n\n")
    print("Total Right: {0} | Total Wrong: {1} | Percent: {2}".format(total_right, total_wrong, (total_right / (total_right + total_wrong))))

    if debug:
        print()
        print()
        print("**Table of Guesses***")
        print("row is correct answer, column is # of times of each guess.")
        print()
        print(" "*10, end=' ')
        for i in range(5):
            print("{0:10}".format(solutions[i]), end=' ')
        print()
        for i in range(5):
            print("{0:10}".format(solutions[i]), end=' ')
            for j in range(5):
                print("{0:10}".format(guesses[i][j]), end=' ')
            print()


                                                                                                                                                                                
