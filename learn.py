#!/usr/bin/env python3

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from random import randrange,shuffle
from scipy.special import expit

alpha = 0.001               # learning rate
debug = False


def get_file_names():

    running_dir = os.getcwd()
    os.chdir("GoogleDraw")
    files = os.listdir()
    files_full_path = []
    os.chdir(running_dir)

    for f in files:
        files_full_path.append(os.path.join(os.getcwd()) + "\\GoogleDraw\\" + f)
    
    return files_full_path


def make_composite_image(filename):

    im1 = np.load(filename)
    rows = im1.shape[0]
    im2 = im1.reshape(rows, 28, 28)
    im3 = im2[:1250, :, :]
    im4 = im3.reshape(25, 50, 28, 28)
    im5 = im4.transpose((0, 2, 1, 3))
    im6 = im5.reshape(700, 1400)
    im7 = 255 - im6
    plt.imsave(filename[44:-4]+".png", im7, cmap="Greys")
    print("Composite Image of", filename[44:-4], "saved as: ", filename[44:-4]+".png")


if __name__ == "__main__":

    #    read file, reshape to workable format
    #    print the first 1250 images of airplanes in a

    files = get_file_names()
    
    images = []
    labels = {}

    n = 0
    for f in files:
        c = np.load(f)
        name = f[64:-4]
        labels[n] = name
        images.append(c)
        n += 1

    smallest = None

    for t in images:
        length = t.shape[0]
        if smallest is None:
            smallest = length
        elif length < smallest:
            smallest = length

    new_canvas = np.zeros(smallest*5*784).reshape(5, smallest, 784)
    
    for i in range(len(images)):
        cropped_image = images[i][:smallest, :]
        new_canvas[i] = cropped_image

    for i in range(5):
        np.random.shuffle(new_canvas[i])

    part = smallest * 4 // 5
    print("Size of Training Data: ", part)
    train = new_canvas[:, :part, :]
    test = new_canvas[:, part:, :]

    np.save("testSet.npy", test)
        
# random starting weights matrix: 
# the values of w are what we are 'learning'

    w1 = np.random.randn(98000).reshape(125, 784)
    w2 = np.random.randn(625).reshape(5, 125)

# some data to keep track of how we are doing
    right = wrong = 0

# training Loop
     
    picklist = [(i, j) for i in range(5) for j in range(part)]

    print("***Training Data***")
    for reps in range(1):
        shuffle(picklist)
        for whichtype, whichimage in picklist:
            x = train[whichtype, whichimage]     # getting 784 pixels for this picture
        
            # make a one-hot vector:
            #      if right answer is 0, the vector is [1,0] (1 in the 0 position)
            #      if right answer is 1, the vector is [0,1] (1 in the 1 position)
            
            y = np.zeros(5)              # make a vector of zeros
            y[whichtype] = 1.0           # set the answer component to 1
            
            # 1st Layer
            
            z1 = np.matmul(w1, x)
            v1 = expit(z1)
            
            # Final Layer
            
            z2 = np.matmul(w2, v1)       # forward propagation to layer 2
            a = expit(z2)                # logistic function

            # error checking
            
            delta2 = a - y                     # error at layer 3: output - answer
            delta2 *= a * (1 - a)
            delta1 = np.matmul(w2.T, delta2)   # error at layer 2: w3^T @ delta3
            
            yans = np.argmax(a)                # which component is larger
            
            if yans == whichtype:
                right += 1
            else:
                wrong += 1

            dw2 = np.outer(delta2,v1)    # make corrections at layer two
            w2 -= alpha * dw2           #
        
            dw1 = np.outer(delta1, x)   # make corrections at layer one
            w1 -= alpha * dw1           #
    
    print("right: {0}, wrong:{1}, percent:{2}".format(right, wrong, right/(right+wrong)))

    np.save("L1-weights.npy", w1)
    np.save("L2-weights.npy", w2)
